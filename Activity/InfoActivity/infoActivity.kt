import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activityInfo.*
 
class InfoActivity : AppCompatActivity() {
 
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activityInfo)
 
        val surname = intent.getStringExtra("surname")
        val name = intent.getStringExtra("name")
        val patronymic = intent.getStringExtra("patronymic")
        val age = intent.getStringExtra("age")?.toIntOrNull()
        val hobby = intent.getStringExtra("hobby")
 
        val infoText = when {
            age == null || age <= 0 -> "Ошибка: некорректный возраст"
            age in 1..17 -> "$surname $name $patronymic, тебе $age лет. Ты еще молод, занимайся своим хобби - $hobby."
            age in 18..59 -> "$surname $name $patronymic, тебе $age лет. Наслаждайся жизнью и занимайся своим хобби - $hobby."
            else -> "$surname $name $patronymic, тебе $age лет. Не забывай делать зарядку каждое утро и занимайся своим хобби - $hobby."
        }
 
        text_info.text = infoText
    }
}
